package redgal_http

import (
	"bytes"
	"image"
	"image/jpeg"
	"io"

	"github.com/disintegration/imaging"
)

func mkThumb(r io.Reader, w, h int) (out io.Reader, err error) {
	img, _, err := image.Decode(r)
	if err != nil {
		return nil, err
	}

	thumb := imaging.Thumbnail(img, w, h, imaging.Lanczos)

	buf := bytes.NewBuffer([]byte{})

	err = jpeg.Encode(buf, thumb, &jpeg.Options{Quality: 51})
	if err != nil {
		return nil, err
	}

	out = buf

	return
}
