package redgal_http

import (
	"log"
	"mime/multipart"
	"net/http"

	"bitbucket.org/colorsocean/redgal"
	"bitbucket.org/colorsocean/response"
	"bitbucket.org/colorsocean/rtool"
	"bitbucket.org/colorsocean/s2"
)

type RedGalHTTP struct {
	//> App-context
	RedGal  *redgal.RedGal
	Storage *s2.Storage

	ThumbWidth  int
	ThumbHeight int

	//> Request-context
	Response response.Response
}

func (this RedGalHTTP) Create(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		Title    string //> (✿◠‿◠)
		Descr    string //> (●´ω｀●)
		ParentID string //> (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
	}

	var resp struct { //> RESPONSE
		Gallery *redgal.GalleryInfo `json:",omitempty"`
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	newID := redgal.Title4ID(req.Title)

	log.Println("New gallery ID:", newID)

	gal := this.RedGal.GetByID(newID)

	if req.Title != "" {
		gal.SetTitle(req.Title)
	}

	if req.Descr != "" {
		gal.SetDescr(req.Descr)
	}

	if req.ParentID != "" {
		gal.SetParent(req.ParentID)
	}

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) Remove(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID string
	}

	var resp struct{}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	err = this.RedGal.RemoveByID(req.ID)
	if err != nil {
		panic(err)
	}

	this.Response.Payload(resp)
}

func (this RedGalHTTP) Get(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID string
	}

	var resp struct {
		Gallery *redgal.GalleryInfo
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) SetTitle(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID    string
		Title string
	}

	var resp struct {
		Gallery *redgal.GalleryInfo
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	gal.SetTitle(req.Title)

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) SetDescr(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID    string
		Descr string
	}

	var resp struct {
		Gallery *redgal.GalleryInfo
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	gal.SetDescr(req.Descr)

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) SetCover(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID      string
		ImageID string
	}

	var resp struct {
		Gallery *redgal.GalleryInfo
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	gal.SetCover(req.ImageID)

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) AddImage(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		GalID    string
		NewImage *multipart.FileHeader
	}

	var resp struct { //> RESPONSE
		Image   *redgal.Image
		Gallery *redgal.GalleryInfo `json:",omitempty"`
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	var newImageFull *s2.FileInfo
	var newImageThumb *s2.FileInfo

	if req.NewImage != nil {
		orig, err := req.NewImage.Open()
		if err != nil {
			panic(err)
		}
		defer orig.Close()
		storeOp := s2.Store{Name: req.NewImage.Filename, MIME: req.NewImage.Header.Get("Content-Type")}
		storeOp.R = orig
		fileOrig, err := this.Storage.Store(storeOp)
		if err != nil {
			panic(err)
		}
		_, err = orig.Seek(0, 0)
		if err != nil {
			panic(err)
		}

		thumb, err := mkThumb(orig, this.ThumbWidth, this.ThumbHeight)
		if err != nil {
			panic(err)
		}
		storeOp.R = thumb
		fileThumb, err := this.Storage.Store(storeOp)
		if err != nil {
			panic(err)
		}

		newImageFull = fileOrig.Info()
		newImageThumb = fileThumb.Info()
	}

	gal := this.RedGal.GetByID(req.GalID)

	resp.Gallery = gal.GetInfo()

	resp.Image = gal.AddImage(redgal.Image{
		Full:  newImageFull,
		Thumb: newImageThumb,
	})
	if err != nil {
		panic(err)
	}

	this.Response.Payload(resp)
}

func (this RedGalHTTP) RemoveImage(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID      string
		ImageID string
	}

	var resp struct {
		Gallery *redgal.GalleryInfo
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	gal.RemoveImage(req.ImageID)

	resp.Gallery = gal.GetInfo()

	this.Response.Payload(resp)
}

func (this RedGalHTTP) GetImage(w http.ResponseWriter, q *http.Request) {
	var req struct { //> REQUEST
		ID      string
		ImageID string
	}

	var resp struct {
		Image *redgal.Image
	}

	_, err := rtool.Read(rtool.Options{Q: q, Out: &req, Response: this.Response, Validate: true})
	if err != nil {
		panic(err)
	}

	gal := this.RedGal.GetByID(req.ID)

	resp.Image = gal.GetImage(req.ImageID)

	this.Response.Payload(resp)
}
